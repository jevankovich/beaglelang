AOPS = -march=armv7-a --warn --fatal-warnings
COPS = -march=armv7-a -mtune=cortex-a8 -mthumb -Wall -Werror -nostdlib -nostartfiles -ffreestanding -fno-builtin -std=c99 -pedantic -Os

include setup.mk

vpath %.c src
vpath %.h src
vpath %.s src
vpath %.o bin

boot/boot.hex: boot.o makefile
	$(ARMGNU)-ld -T linker.ld bin/*.o -o bin/boot.elf
	$(ARMGNU)-objdump -D bin/boot.elf > list/boot.list
	$(ARMGNU)-objcopy bin/boot.elf -O srec boot/boot.hex

boot.o: boot.s mem.o io.o main.o makefile
	$(ARMGNU)-as $(AOPS) src/boot.s -o bin/boot.o

main.o: main.c lisp.o makefile
	$(ARMGNU)-gcc -c $(COPS) src/main.c -o bin/main.o

%.o: %.c %.h makefile
	$(ARMGNU)-gcc -c $(COPS) $< -o bin/$*.o

io.o: serial.o

gc.o: mem.o

lisp.o: lisp_types.h gc.o serial.o

clean:
	rm -f bin/* list/* boot/*

install: boot/boot.hex
	echo loads > $(SERIAL)
	sleep 0.1
	cat boot/boot.hex > $(SERIAL)
	sleep 1

run: install
	echo "go 0x80300000" > $(SERIAL)

dirs:
	mkdir -p bin list boot
