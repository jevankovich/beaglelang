# This is the prefix for the gnu arm toolchain
# arm-none-eabi on FreeBSD 10
# arm-linux-gnu on Fedora 20
ARMGNU = arm-none-eabi

# This is the dialout device.
# Using a usb adapter on FreeBSD 10, this is /dev/cuaU0
# Using a usb adapter on Fedora 20, this is /dev/ttyUSB0
SERIAL = /dev/cuaU0
