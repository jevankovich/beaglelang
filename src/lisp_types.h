#ifndef LISP_TYPES_H
#define LISP_TYPES_H

#include <stddef.h>

enum type {
	STRING,
	SYMBOL,
	CONS,
	NUM,
	CHAR
};

struct string {
	size_t length;
	char arr[];
};

union typed {
	struct string *as_str;
	struct string *as_sym;
	struct cons *as_cons;
	int as_int;
	char as_char;
};

struct cons {
	enum type tcar, tcdr;
	union typed car, cdr;
};

#endif
