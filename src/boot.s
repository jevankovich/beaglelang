// Play nicely with the linker
.section .text

.arm

.align 4
.global _start
// Entry point!
_start:
   // Initialize the stack
   ldr   sp, =stack_top
   // Initialize the memory allocator
   ldr   r0, =heap_start
   blx   init_mem
   // Do the thing!
   blx   main
   // Prompt the user/developer monkey to reset the system
   ldr   r0, =str
   blx   write
   blx   read_char
   b     reset

str:
.asciz "\r\nPlease enter a character to reset."
