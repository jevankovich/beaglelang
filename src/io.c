/* io.c
 * This builds on the serial module to implement more complex
 * and useful io over serial.
 */

#include "io.h"
#include "serial.h"

static const char* hex = "0123456789abcdef";

// Prints a single byte as two hex characters
void hex_byte(uint8_t byte) {
	write_char(hex[(byte >> 4) & 0xF]);
	write_char(hex[ byte       & 0xF]);
}

// Prints a single machine word as 8 hex characters
void hex_word(uint32_t word) {
	for (int i = 7; i >= 0; i--) {
		write_char(hex[(word >> (i * 4)) & 0xF]);
	}
}

/* Prints an array of machine words.
 * Each word is separated by spaces and eight words are printed per line
 */
void hex_words(const uint32_t* words, size_t len) {
	for (int i = 0; i < len; i++) {
		if (i % 8) { write_char(' '); }
		else       { write_char('\r'); write_char('\n'); }
		hex_word(*(words + i));
	}
}

// Writes characters over serial until a null byte is encountered
void write(const char* out) {
	int i = 0;
	while (1) {
		if (out[i] == '\0') { break; }
		write_char(out[i]);
		i++;
	}
}

// Write characters over serial until len characters have been written
void writen(const char* out, size_t len) {
	for (int i = 0; i < len; i++) {
		write_char(out[i]);
	}
}