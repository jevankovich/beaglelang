/* serial.c
 * This module defines serial io primitives.
 * The functionality exposed by this module is single character
 * reading and writing.
 */

#include <stdint.h>
#include "serial.h"

// const pointer to volatile uint32_t
static uint32_t volatile * const UART_BASE = (uint32_t*)0x49020000; // Pointer can't change, value can
static uint32_t volatile * const UART_LSR  = (uint32_t*)0x49020014;

// Reads a single char from serial, prevents underflow
char read_char() {
	while (!(*UART_LSR & 0x1)) {} // Wait until data is available
	return (char)(*UART_BASE); // Return data
}

// Writes a single char to serial, prevents overflow
void write_char(char c) {
	while (!(*UART_LSR & (0x1 << 5))) {} // Wait until FIFO is empty
	*UART_BASE = c;
}