#ifndef MEM_H
#define MEM_H

#include <stddef.h>

void  init_mem(void* );
void *alloc   (size_t);
void  free    (void* );

#endif
