#ifndef IO_H
#define IO_H

#include <stddef.h>
#include <stdint.h>

void hex_byte(uint8_t);
void hex_word(uint32_t);
void hex_words(const uint32_t*, size_t);

void write(const char*);
void writen(const char*, size_t);

#endif