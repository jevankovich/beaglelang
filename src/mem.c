#include <stdint.h>
#include "mem.h"
#include "io.h"

char * const MEM_BASE = (char*)0x80000000;

struct header {
	uint32_t size_class : 5;  // The size class of this cell
	uint32_t offset     : 26; // The offset, in `sizes' that the next cell
	                          // is from MEM_BASE
};

// Returns a pointer to the header referenced by a given header
static inline struct header *next(struct header h) {
	return (struct header *)(0x80000000 + (1 << (3 + h.size_class)) * h.offset);
}

static inline uint32_t correct_offset(struct header *h, uint32_t size_class) {
	return ((uint32_t)h - (uint32_t)MEM_BASE) << (3 + size_class);
}

static inline struct header *address_of(uint32_t size_class, uint32_t offset) {
	return (struct header *) ((offset << (3 + size_class)) + MEM_BASE);
}

// These first headers will not have meaningful data in the free field.
// If the offset field is zero, the list is empty.
static struct header headers[26];

void init_mem(void* heap_start) {
	char *free_end = (char *)0xA0000000;
	size_t heap_size = (size_t)free_end - (size_t)heap_start;

	for (uint32_t i = 0; i < 26; i++) {
		size_t size = 1 << (28 - i); 
		uint32_t size_class = 25 - i;
		if (size & heap_size) {
			free_end -= size;
			*((struct header *)free_end) = 
				(struct header) {size_class, 0};
			headers[size_class] = (struct header) {size_class,  
				correct_offset((struct header *)free_end, size_class)};
		} else {
			headers[size_class] = (struct header) {size_class, 0};
		}
	}
}

// This assumes that the given size class has at least one node and
// the class below has none
void split(uint32_t size_class) {
	struct header *high, *low = next(headers[size_class]);
	headers[size_class].offset = low->offset;
	size_class--;

	low->size_class = size_class;
	headers[size_class].offset = 
		((uint32_t)low - (uint32_t)MEM_BASE) >> (3 + size_class);
	
	high = (struct header*)((headers[size_class].offset << (3 + size_class)) + MEM_BASE);
	*high = (struct header) {size_class, 0};

	low->offset = headers[size_class].offset + 1;
}

void *alloc(size_t size) {
	size = (size + 3) & ~3; // Round up to the nearest 4-byte boundary
	size += 4; // Increment to accomodate header size
	// Perform what is essentially an integer log2, rounding up
	// This decides which chunk size to use
	uint32_t size_class = 0;
	while (size != 0) {
		size >>= 1;
		size_class++;
	}
	size_class -= 3;

	if (headers[size_class].offset != 0) {
		struct header *ret = next(headers[size_class]);
		headers[size_class].offset = ret->offset;
		return ret + 1;
	}

	{
		uint32_t i = size_class + 1;
		while (i > size_class && i < 26) {
			if (headers[i].offset == 0) i++;
			else split(i--);
		}
	}

	if (headers[size_class].offset != 0) {
		struct header *ret = next(headers[size_class]);
		headers[size_class].offset = ret->offset;
		return ret + 1;
	}

	return NULL;
}

void free(void *data) {

}

