/* mem.c
 * This module defines operations relating to memory management.
 * Expected functionality:
 * 	ability to allocate a new, arbitrarily large piece of memory
 * 	ability to free allocated memory
 */

#include "mem.h"

// Header for free list
typedef struct ml_struct {
	// Size indicates size available to use once the "next" pointer is stripped
	size_t size;
	struct ml_struct* next;
} memlist;

// Header for allocated chunks is a single size_t indicating the size of the allocation

static memlist* freelist;

// Initializes the heap to contain all available memory
// from the end of the stack to the end of memory
void init_mem(void* heap_start) {
	// Set free to memory after program
	freelist = (memlist*)heap_start;
	// Memory after heap_start is mem_base + mem_size - heap_start - sizeof(size_t)
	// The space occupied by the "next" pointer is usable at allocation time
	freelist->size = 0xA0000000 - (size_t)heap_start - sizeof(size_t);
	// Everything in 4 byte chunks
	freelist->size = (freelist->size + 3) & ~0x3; // 0x3 == 0b11
	freelist->next = NULL;
}

// Takes the first available chunk of sufficient size in the heap
void* alloc(size_t size) {
	if (size == 0) { return NULL; }
	// Everything is dealt with in 4 byte chunks
	size = (size + 3) & ~0x3; // 0x3 == 0b11
	// Find the first free block large enough to hold the requested size
	// Return NULL if one isn't found
	memlist* prev = NULL;
	memlist* curr = freelist;
	while (curr != NULL && curr->size < size) { curr = (prev = curr)->next; }
	if (curr == NULL) { return NULL; }
	if ((curr->size - size) < sizeof(memlist)) {
		size = curr->size;
	} else {
		// Create new entry and add it to the list
		memlist* new = (memlist*)((char*)curr + sizeof(size_t) + size);
		new->size = curr->size - size;
		new->next = curr->next;
		curr->next = new;
	}
	// Remove curr;
	if (prev == NULL) { freelist   = curr->next; }
	else              { prev->next = curr->next; }
	
	curr->size = size;
	return ((char*)curr) + sizeof(size_t);
}

// Frees a given chunk of memory as given by alloc.
// If given a pointer that wasn't given by alloc, behavior is undefined (nasty)
void free(void* loc) {
	// "rebuild" memlist reference
	memlist* toadd = (memlist*)((char*)loc - sizeof(size_t));
	// Find where to add this node to the list
	if (freelist == NULL) {
		freelist = toadd;
		toadd->next = NULL;
		return;
	}
	memlist* prev = NULL;
	memlist* curr = freelist;
	while (curr != NULL && curr < toadd) { curr = (prev = curr)->next; }
	toadd->next = curr;
	if (prev == NULL) { freelist = toadd;   }
	else              { prev->next = toadd; }
	// If prev and curr (the node after toadd) are consecutive with toadd, they must be merged
	// If toadd mushes into curr, merge them
	if (curr != NULL && (char*)toadd + toadd->size + sizeof(memlist*) == (char*)curr) {
		toadd->next = curr->next;
		toadd->size = toadd->size + curr->size;
	}
	// If prev mushes into toadd, merge them
	if (prev != NULL && (char*)prev + prev->size + sizeof(memlist*) == (char*)toadd) {
		prev->next = toadd->next;
		prev->size = prev->size + toadd->size;
	}
}

