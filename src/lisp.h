#ifndef LISP_H
#define LISP_H

#include "lisp_types.h"

struct string *inputln();
struct form   *read   (struct string *);
struct form   *eval   (struct form   *);
void           print  (struct form   *);

#endif
