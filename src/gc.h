#ifndef GC_H
#define GC_H

#include "lisp_types.h"

void     *gc_alloc(enum   type  );
void      collect (struct cons *);
enum type typeof  (       void *);

#endif
