#include <stddef.h>

#include "gc.h"
#include "mem.h"

void *gc_alloc(enum type t) {
	// STUB!!
	size_t size;
	switch (t) {
		case STRING:
		case SYMBOL:
			size = sizeof (struct string);
			break;
		case CONS:
			size = sizeof (struct cons);
			break;
		case NUM:
			size = sizeof (int);
			break;
		default:
			size = 0;
	}

	return alloc(size);
}

void collect(struct cons *cons) {
	// STUB!!
	return;
}

enum type typeof(void *object) {
	// STUB!!
	return 0;
}
